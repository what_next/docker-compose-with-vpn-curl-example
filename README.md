Repo to show how to use `docker-compose up` when using a vpn.

When using a docker-compose.yaml with a vpn, I get the following error: `ERROR: could not find an available, non-overlapping IPv4 address pool among the defaults to assign to the network`. To fix this issue, add `network_mode: bridge` to your service and this error will go away. See my `docker-compose.yaml` as a reference.

Source: https://stackoverflow.com/questions/43720339/docker-error-could-not-find-an-available-non-overlapping-ipv4-address-pool-am
(Stefan van den Akker)

The container will still be using your VPN.
